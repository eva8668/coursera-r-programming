pollutantmean <- function(directory, pollutant, id = 1:332) {
        ## 'id' is an integer vector indicating the monitor ID numbers
        ## to be used
        means <- c(length(id))
        counts <- c(length(id))
        
        for (index in seq_along(id)) {
                ## 'directory' is a character vector of length 1 indicating
                ## the location of the CSV files
                f <- sprintf("%03d.csv", id[index])
                csvfile <- file.path(directory, f)
                
                if (file.exists(csvfile) == TRUE) {
                        ## 'pollutant' is a character vector of length 1 indicating
                        ## the name of the pollutant for which we will calculate the
                        ## mean; either "sulfate" or "nitrate".
                        csv <- read.csv(csvfile)
                        n <- csv[[pollutant]]
                        
                        means[index] <- mean(n, na.rm = TRUE)
                        counts[index] <- length(n[!is.na(n)])
                }
        }
        
        ## Return the mean of the pollutant across all monitors list
        ## in the 'id' vector (ignoring NA values)
        ## NOTE: Do not round the result!
        ##print(counts)
        ##print(means * counts)
        if (sum(counts, na.rm = TRUE) == 0) {
                0
        } else {
                sum(means * counts, na.rm = TRUE) / sum(counts, na.rm = TRUE)
        }
        

}
